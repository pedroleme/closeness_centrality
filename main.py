#!/usr/bin/python
# -*- coding: utf-8 -*-

# using python logging system
import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
logger = logging.getLogger()

# mainly for file handling
import sys, os
import shutil

# regex module
import re

# bottle is used to implement some REST calls
# if it cannot be imported, a flag is set and the script runs on simple mode
bottle_available = False
try:
    import bottle
    bottle_available = True
except:
    logger.warning('could not import bottle, reverting to simple')

# hardcoded filenames for now
# a 'work' file and a default one
filename_def = './data/edges'
filename = './data/edges2'

# reads all edges from the file and generates the vertices list
def read_edges_file():
    with open(filename, 'r') as f:
        # read all lines and separate vertices of each edge
        edges = [line.strip().split(u' ') for line in f if len(line.strip())]

    # generates a list of unique vertices (that's what set() is used here)
    vertices = list(set([x for s in edges for x in s]))

    return edges, vertices

# just makes sure there's a file edges2 (and overwrites it if asked)
def set_edges_file(reset_to_default=False):
    if reset_to_default or not os.path.isfile(filename):
        shutil.copyfile(filename_def, filename)

# add an edge (or a list of) to the file
# it is not checking for duplicates
def add_edges_to_file(edges):
    with open(filename, 'a') as f:
        for edge in edges:
            f.write('\n'+edge)

# generates and fulfill the graph with the values from edges
def generate_graph(edges, vertices):
    len_vertices = len(vertices)

    # converts edges to numeric index (easier to work on)
    converted_edges = [[vertices.index(x) for x in s] for s in edges]

    # generates a matrix len_vertices x len_vertices using list comprehensions
    try: # try/except so it runs on python2 or python3
        graph = [[float('inf') if x!=y else 0 for y in xrange(0,len_vertices)] for x in xrange(0,len_vertices)]
    except NameError as e:
        graph = [[float('inf') if x!=y else 0 for y in range(0,len_vertices)] for x in range(0,len_vertices)]

    # save to the matrix the distances 1 of connected vertices
    # considering an undirected graph
    for x,y in converted_edges:
        graph[x][y] = graph[y][x] = 1

    return graph

# using Floyd–Warshall algorithm
def shortest_paths_floyd_marshall(graph):
    len_vertices = len(graph)

    try: # try/except so it runs on python2 or python3
        for k in xrange(0, len_vertices):
            for i in xrange(0, len_vertices):
                for j in xrange(0, len_vertices):
                    if graph[i][j] > graph[i][k] + graph[k][j]:
                        graph[i][j] = graph[j][i] = graph[i][k] + graph[k][j]
    except NameError as e:
        for k in range(0, len_vertices):
            for i in range(0, len_vertices):
                for j in range(0, len_vertices):
                    if graph[i][j] > graph[i][k] + graph[k][j]:
                        graph[i][j] = graph[j][i] = graph[i][k] + graph[k][j]

def closeness_rank(graph, vertices):
    # creates a list of [vertex, dict(information of centrality)] of each vertex
    vfc = [[vertex, {'_vertex': vertex, 'farness': sum(graph[idx]), 'closeness': 1.0/sum(graph[idx])}] for idx,vertex in enumerate(vertices)]

    # ordering the vertices by their closenesses
    vfc_ordered = sorted(vfc, key=lambda x: x[1]['closeness'], reverse=True)

    return vfc_ordered

# calls all the functions in order to generate the centrality metric of vertices
def full_edges_to_closeness():
    edges, vertices = read_edges_file()

    graph = generate_graph(edges, vertices)

    shortest_paths_floyd_marshall(graph)

    vfc_ordered = closeness_rank(graph, vertices)

    return vfc_ordered

# if file is called directly, run it
if __name__ == u'__main__':
    # check argument in command line to check if edges2 should be reset
    reset_to_default = True if '--reset_edges' in sys.argv else False

    # create or reset edges2
    set_edges_file(reset_to_default=reset_to_default)

    # if we only want a simple run or if bottle is not available
    if not bottle_available or '--simple' in sys.argv:
        from pprint import pprint
        pprint(full_edges_to_closeness())
    else:
        # starting the REST app
        app = bottle.Bottle()

        # returns the centrality list
        @app.route('/centrality')
        def centrality():
            try:
                return {'result': 'OK', 'payload': full_edges_to_closeness()}
            except Exception as e:
                return {'result': 'ERROR', 'message': 'error trying to generate centrality metric'}

        # adds new edge(s) to the edges2 file
        @app.route('/edge', method='POST')
        def add_edge():
            try:
                # read data sent as 'edge'
                edges = bottle.request.POST.get('edge')

                # remove invalid entries and send to the function where edges will be saved
                add_edges_to_file([x.strip() for x in edges.split(r'\n') if re.search(r'\d+\s\d+', x.strip())])

                return {'result': 'OK'}
            except Exception as e:
                logger.exception('error trying to add a new edge')

                return {'result': 'ERROR', 'message': 'error trying to add new edge'}

        # let's run
        bottle.run(app, host='localhost', port=8080)