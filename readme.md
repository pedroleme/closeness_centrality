# Centrality metrics #

Code in this repository implements the Closeness/Farness metric on an undirected graph in python.

## Installation on Ubuntu 14.04 (Python 2.7) ##
```
#!bash
sudo apt-get install python python-pip
sudo pip install bottle
git clone git@bitbucket.org:pedroleme/closeness_centrality.git
```

## Running ##
```
#!bash
cd closeness_centrality
python main.py [--reset_edges] [--simple]
```

### Options ###

If, for any reason, you want to revert edges back to default, use
```
#!bash
--reset_edges
```

And, if you do not want to run the REST server and only want to see the centrality printed, use
```
#!bash
--simple
```

## REST endpoints ##

Once started, server will listen on 'http://localhost:8080/'

To create a new edge between vertices - X,Y numbers
```
#!bash
curl -d "edge=X Y" http://localhost:8080/edge
```

And to get a list of the calculated centrality metric go for 'http://localhost:8080/centrality' or use
```
#!bash
curl http://localhost:8080/centrality
```

## Considerations ##

* A text file, as the original edges file, is used as base for the raw data of the graph
* The list-based matrix approach is not indicated performance-wise (more suitable alternatives for python include numpy), but in order to avoid the need for other modules lists were used here
* A considerable part of the list iterations was done using list comprehensions, but could be expanded without problems